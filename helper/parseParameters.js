/**
 * Created by Aria on 2016-07-12.
 */

var mongoose = require('mongoose');

//배열 형식의 필요한 parameters 보냄

module.exports = function( parameters )
{
    return function( req, res, next )
    {
        req.parseParams = {};
        for( var i = 0, max = parameters.length; i < max; ++i)
        {
            var param = parameters[i];

            var isRequire = false; // 기본값

            //정의 확인
            //확인된 정의가 있으면 불러옴
            if( param.require !== undefined )
            {
                isRequire = param.require;
            }

            var value = req.body[param.name];

            if( value === undefined || value === null || value === '' )
            {
                if( isRequire === true )
                {
                    return next( new Error( 'required parameter :'  + param.name ));
                }
            }

            else
            {
                switch (param.type)
                {
                    case 'string':
                    {
                        //
                        break;
                    }

                    case 'objectId':
                    {
                        try
                        {

                        }
                        catch( err )
                        {
                            return next( new Error( 'invalid parameter type : ' + param.type ));
                        }
                        break;
                    }

                    case 'int':
                    {
                        value = parseInt( value, 10 );
                        break;
                    }

                    case 'float':
                    {
                        value = parseFloat( value );
                        break;
                    }

                    case 'json':
                    {
                        if( typeof( value ) == 'string' )
                        {
                            value = JSON.parse( value );
                        }
                        break;
                    }

                    default :
                        log.debug('undefined parse type : ' + param.type );
                }
                req.parseParams[ param.name ] = value;
            }
        }
        return next();
    }
};
