/**
 * Created by aheanuri on 2015. 3. 29..
 */
var fs = require( 'fs' );

var trim = function ( str )
{
    //좌우 공백 제거
    str = str.replace( /(^\s*)|(\s*$)/g, '' );
    return str;
}
module.exports = function ( callback )
{
    var apiParameters = {};
    var controllersPath = __dirname + '/../controllers/';
    var files = fs.readdirSync( controllersPath );

    files.forEach( function ( fileName )
        {
            if ( fileName.match( /\Controller.js/g ) === null )
            {
                return false;
            }

            var file = fs.readFileSync( controllersPath + fileName, 'utf8' );

            while ( true )
            {
                var startIndex = file.search( /\/\*\*\*/ ); // /***

                if ( startIndex === -1 || startIndex === null )
                {
                    break;
                }

                file = file.substr( startIndex );

                var endIndex = file.search( /\*\*\*\// ); // ***/

                var sub = file.substr( 0, endIndex + 4 ); // ***/ 4개 더..

                file = file.substr( endIndex + 4 );


                //sub에 저장된 내용을 분석 한다.

                //@api가 있는지 확
                if ( sub.search( /@api/ ) === null )
                {
                    continue;
                }

                var api = {};
                api.parameters = [];

                var lines = sub.split( '\n' );
                var tempParam;

                lines.forEach( function ( line )
                    {
                        //각 요소를 추출하여 넣어버린다.

                        if ( line.search( /@category/ ) > 0 )
                        {
                            api.category = trim( line.replace( '@category', '' ) );
                            return false;
                        }
                        if ( line.search( /@name/ ) > 0 )
                        {
                            api.name = trim( line.replace( '@name', '' ) );
                            return false;
                        }

                        if ( line.search( /@method/ ) > 0 )
                        {
                            api.method = trim( line.replace( '@method', '' ) );
                            return false;
                        }

                        if ( line.search( /@version/ ) > 0 )
                        {
                            api.version = trim( line.replace( '@version', '' ) );
                            return false;
                        }

                        if ( line.search( /@auth/ ) > 0 )
                        {
                            var temp = trim( line.replace( '@auth', '' ) );

                            if ( temp == "true" || temp == "True" )
                            {
                                api.auth = true;
                            }
                            else if ( temp == "false" || temp == "False" )
                            {
                                api.auth = false;
                            }
                            else
                            {
                                api.auth = false;
                            }

                            return false;
                        }

                        if ( line.search( /@desc/ ) > 0 )
                        {
                            api.desc = trim( line.replace( '@desc', '' ) );
                            return false;
                        }

                        if ( line.search( /@param/ ) > 0 )
                        {
                            line = trim( line.replace( '@param', '' ) );

                            tempParam = line.split( ' ' );

                            var name = tempParam[0];
                            var type = tempParam[1];
                            var required = tempParam[2];
                            var desc = '';

                            for ( var i = 3, max = tempParam.length; i < max; ++i )
                            {
                                desc += tempParam[i] + ' ';
                            }

                            api.parameters.push(
                                {
                                    name : name,
                                    type : type,
                                    required : required,
                                    desc : desc
                                }
                            );

                            return false;
                        }

                    }
                );

                //해당 카테고리가 없으면 추가!
                if ( apiParameters[ api.category ] === undefined )
                {
                    apiParameters[ api.category ] = [];
                }

                apiParameters[ api.category ].push( api );
            }
        }
    );

    return callback( apiParameters );
};