/**
 * Created by Aria on 2016-07-12.
 */

var async = require('async');

var dataTable = require( './../models/dataTable' );

var bossProbabilityModel = require('./../models/localData/bossProbability');
var boxProbabilityModel = require('./../models/localData/boxProbability');
var itemLocalModel = require('./../models/localData/itemLocal');
var boxTypeModel = require('./../models/localData/boxType');
var warriorStatusModel = require('./../models/localData/warriorStatus');
var magicianStatusModel = require('./../models/localData/magicianStatus');
var archerStatusModel = require('./../models/localData/archerStatus');
var experienceModel = require('./../models/localData/experience');

//redis
var cachedData = {};

var Load = function( callback )
{
    dataTable.Model.find( {} ).sort( {revision : -1} ).limit( 1 ).read( 'p' ).exec(
        function ( findError, data )
        {
            if ( findError )
            {
                if ( callback )
                  return callback( findError );
            }

            var revision = 0;

            if ( data.length > 0 )
            {
                revision = data[0].revision;
                data = data[0].data;
            }
            else
            {
                data = {};
            }

            Update( data, revision,  function ( err )
                {
                    if ( !err )
                    {
                        if ( callback )
                           return  callback ( null );
                    }
                }
            );
        }
    );
}
var Init = function( data, revision, callback )
{
    Update( data, revision, callback );
}

var Update = function( data, revision, callback )
{
    //json
    StaticData = {};

    async.waterfall(
        [
            //아이템 로컬
            function( wcallback )
            {
                StaticData['item'] = [];

                if( data.item && data.item.length > 0 )
                {
                    data.item.forEach(function(itemLocalData){

                            var itemLocal = new itemLocalModel.Model();
                            itemLocal.parse( itemLocalData );

                            StaticData['item'].push( itemLocal );

                        }
                    );

                    console.log( 'item Init : ' + StaticData['item'].length );
                }
                else
                {
                    console.log( 'dont item sheet' );
                }

                return wcallback( null );
            },
            //보스 드랍 아이템
            function (wcallback) {

                StaticData['bossProbability'] = [];

                if( data.bossProbability && data.bossProbability.length > 0 )
                {
                    data.bossProbability.forEach( function( bossProbabilityData ){

                            var bossProbability = new bossProbabilityModel.Model();
                            bossProbability.parse(bossProbabilityData);

                            StaticData['bossProbability'].push( bossProbability );
                        }
                    );

                    console.log('bossProbability Init : ' + StaticData['bossProbability'].length );
                }
                else
                {
                    console.log('dont bossProbability sheet');
                }

                return wcallback( null );
            },
            //박스 타입
            function( wallback ){
                StaticData['boxType'] = [];

                if( data.boxType && data.boxType.length > 0 )
                {
                    data.boxType.forEach( function( boxTypeData ){

                            var boxType = new boxProbabilityModel.Model();
                            boxType.parse(boxTypeData);

                            StaticData['boxType'].push( boxType );
                        }
                    );

                    console.log('boxType Init : ' + StaticData['boxType'].length );
                }
                else
                {
                    console.log( ' dont boxType sheet' );
                }

                return wallback( null );
            },
            //박스 별 드랍 아이템
            function (wcallback) {

                StaticData['boxProbability'] = [];

                if( data.boxProbability && data.boxProbability.length > 0 )
                {
                    data.boxProbability.forEach( function( boxProbabilityData ){

                            var boxProbability = new boxProbabilityModel.Model();
                            boxProbability.parse(boxProbabilityData);

                            StaticData['boxProbability'].push( boxProbability );
                        }
                    );

                    console.log('boxProbability Init : ' + StaticData['boxProbability'].length );
                }
                else
                {
                    console.log('dont boxProbability sheet');
                }

                return wcallback( null );
            },
            //용병 Status
            function(wcallback)
            {
                //전사용병
                StaticData['warriorStatus'] = [];

                if( data.warriorStatus && data.warriorStatus.length > 0 )
                {
                    data.warriorStatus.forEach( function( warriorStatusData ){

                            var warriorStatus = new warriorStatusModel.Model();
                            warriorStatus.parse(warriorStatusData);

                            StaticData['warriorStatus'].push( warriorStatus );
                        }
                    );

                    console.log('warriorStatus Init : ' + StaticData['warriorStatus'].length );
                }
                else
                {
                    console.log('dont warriorStatus sheet');
                }

                //마법사용병
                StaticData['magicianStatus'] = [];

                if( data.magicianStatus && data.magicianStatus.length > 0 )
                {
                    data.magicianStatus.forEach( function( magicianStatusData ){

                            var magicianStatus = new magicianStatusModel.Model();
                            magicianStatus.parse(magicianStatusData);

                            StaticData['magicianStatus'].push( magicianStatus );
                        }
                    );

                    console.log('magicianStatus Init : ' + StaticData['magicianStatus'].length );
                }
                else
                {
                    console.log('dont magicianStatus sheet');
                }

                //궁수용병
                StaticData['archerStatus'] = [];

                if( data.archerStatus && data.archerStatus.length > 0 )
                {
                    data.archerStatus.forEach( function( archerStatusData ){

                            var archerStatus = new archerStatusModel.Model();
                            archerStatus.parse(archerStatusData);

                            StaticData['archerStatus'].push( archerStatus );
                        }
                    );

                    console.log('archerStatus Init : ' + StaticData['archerStatus'].length );
                }
                else
                {
                    console.log('dont archerStatus sheet');
                }

                return wcallback( null );
            },
            //레벨업당 필요 경험치
            function( wcallback )
            {
                StaticData['experience'] = [];

                if( data.experience && data.experience.length > 0 )
                {
                    data.experience.forEach(function (expData)
                        {
                            var experience = new experienceModel.Model();
                            experience.parse(expData);

                            StaticData['experience'].push(experience);
                        }
                    );

                    console.log('experience Init : ' + StaticData['experience'].length );
                }
                else
                {
                    console.log('dont experience sheet');
                }

                return wcallback(null);
            }
        ], function( err )
        {
            cachedData = {
                revision : revision,
                data : StaticData
            };

            if ( callback )
                return callback(err);
        }
    );
}

var Get = function ()
{
    return cachedData;
}

module.exports = {
    Update : Update,
    Init : Init,
    Get : Get,
    Load : Load
}