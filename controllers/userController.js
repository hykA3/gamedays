/**
 * Created by Aria on 2016-07-12.
 */

var mongoose = require( 'mongoose' );
var parseParameters = require( '../helper/parseParameters');
var character = require( '../models/character');
var mercenaryStatus = require('../models/mercenaryStatus');
var async = require( 'async' );

module.exports = function( app )
{
    /***
     @api
     @category user
     @name newUser
     @method POST
     @version 1.0

     @param deviceId string required 계정 아이디
     @param nickName string required 닉네임

     @auth false
     @desc Create
     ***/

    app.post(
        '/user/newUser',
        parseParameters(
            [
                {name : 'deviceId', type : 'string', require : true },
                {name : 'nickName', type : 'string', require : true }
            ]
        ),
        function ( req, res, next ) {

            var deviceId = req.parseParams.deviceId;
            var nickName = req.parseParams.nickName;

            var outputCharacter = {};
            async.waterfall(
                [
                    //아이디, 닉네임 중복체크
                    function( wcallback )
                    {
                        character.Model.find({ deviceId : deviceId }, function (err, document)
                            {
                                if( err )
                                {
                                    return wcallback( err );
                                }

                                if( document && document.length > 0 )
                                {
                                    return wcallback( new Error('OverlabId') );
                                }

                                return wcallback( null );
                            }
                        );
                    },
                    function( wcallback )
                    {
                        character.Model.find({ nickName : nickName }, function (err, document)
                            {
                                if( err )
                                {
                                    return wcallback( err );
                                }

                                if( document && document.length > 0 )
                                {
                                    return wcallback( new Error('OverlabName') );
                                }

                                return wcallback( null );
                            }
                        );
                    },
                    //생성
                    function( wcallback )
                    {
                        var newUser = new character.Model();

                        newUser.deviceId = deviceId;
                        newUser.nickName = nickName;

                        newUser.save( function( err, newDocument )
                            {
                                if( err )
                                {
                                    return wcallback( new Error(err) );
                                }

                                outputCharacter = JSON.parse(JSON.stringify(newDocument));
                                return wcallback( null );
                                //res.result.character = newDocument;
                                //return wcallback( null );
                            }
                        );
                    },
                    //용병생성
                    function (wcallback)
                    {
                        outputCharacter.mercenaryStatus = [];

                        var count = 0;
                        async.whilst(
                            function () { return count < 3; },
                            function (callback) {
                                var newMercenary = new mercenaryStatus.Model();

                                newMercenary.deviceId = deviceId;
                                newMercenary.code = count+1;
                                newMercenary.type  = count+1;

                                newMercenary.save(function (err, newDocument)
                                    {
                                        if (err)
                                        {
                                            return callback( new Error(err) );
                                        }

                                        //res.result.mercenaryStatus += newDocument;
                                        outputCharacter.mercenaryStatus.push (newDocument);

                                        count++;
                                        return callback(null);
                                    }
                                );
                            },
                            function (err) {
                                return wcallback( err);
                            }
                        );
                    }
                ],
                function ( err )
                {
                    res.result.character = outputCharacter;
                    return next( err );
                }
            );
        }

    );

    /***
     @api
     @category user
     @name login
     @method POST
     @version 1.0

     @param deviceId string required 계정 아이디

     @auth false
     @desc Login
     ***/

    app.post(
        '/user/login',
        parseParameters(
            [
                {name : 'deviceId', type : 'string', require : true }
            ]
        ),

        function( req, res, next )
        {
            var deviceId = req.parseParams.deviceId;
            var outputCharacter = {};
            async.waterfall(
                [
                    //유저정보
                    function( wcallback )
                    {
                        character.Model.find({ deviceId : deviceId },
                            function(err, document)
                            {
                                if( err )
                                {
                                    return wcallback( err );
                                }

                                if( document && document.length > 0 )
                                {
                                    outputCharacter = JSON.parse(JSON.stringify(document[0]));
                                    return wcallback( null );
                                }

                                return wcallback( new Error('notFound') );
                            }
                        );
                    },
                    //유저가 가지고 있는 용병의 정보
                    function (wcallback)
                    {
                        mercenaryStatus.Model.find( { deviceId : deviceId },
                            function(err, document)
                            {
                                if( err )
                                {
                                    return wcallback( err );
                                }

                                if( document && document.length > 0 )
                                {
                                    outputCharacter.mercenaryStatus = document;
                                    return wcallback( null );
                                }
                            }
                        );
                    }
                ],
                function( err  )
                {
                    res.result.character = outputCharacter;
                    return next( err );
                }
            );


        }
    );
}