/**
 * Created by Aria on 2016-07-12.
 */

var express = require( 'express' );
var mongoose = require( 'mongoose' );
var async = require( 'async' );
var parseParameters = require( '../helper/parseParameters' );
var dataTable = require( '../models/dataTable' );
//var staticData = require( '../helper/staticData' );
var localData = require( '../helper/localData' );

module.exports = function ( app )
{
    /***
     @api
     @category dataTable
     @name upload
     @method POST
     @version 1.0

     @param dataTable data required dataTable json...

     @auth false
     @desc dataTable을 업로드 한다.
     ***/
    app.post(
        '/dataTable/upload',
        function ( req, res, next )
        {
            console.log( 'dataTable upload..!' );

            if ( typeof(req.body.dataTable) == 'string' )
            {
                req.body.dataTable = JSON.parse( req.body.dataTable );
            }

            async.waterfall( [
                    function ( wcallback )
                    {
                        dataTable.Model.find( {} ).select( 'revision' ).sort( {revision : -1} ).limit( 1 ).read( 'p' ).exec(
                            function ( err, docs )
                            {
                                if ( err )
                                {
                                    return wcallback( new Error( err ) );
                                }

                                if ( !docs )
                                {
                                    return wcallback( null, 0 );
                                }

                                if ( docs.length > 0 )
                                {
                                    return wcallback( null, docs[0].revision + 1 );
                                }
                                else
                                {
                                    return wcallback( null, 0 );
                                }
                            }
                        );
                    }
                    , function ( revision, wcallback )
                    {
                        console.log( 'dataTable revision : ' + revision );

                        var newDataTable = new dataTable.Model();

                        newDataTable.revision = revision;

                        newDataTable.data = req.body.dataTable;


                        newDataTable.save( function ( err, newDocument )
                            {
                                if ( err )
                                {
                                    return wcallback( err );
                                }
                                return wcallback( null, newDataTable, newDataTable.revision );
                            }
                        );
                    },
                    //, function ( data, wcallback )
                    //{
                    //    staticData.Update( data.data, function ( err )
                    //        {
                    //            return wcallback( err, data.data, data.revision );
                    //        }
                    //    );
                    //},
                    function ( data, revision, wcallback)
                    {
                        localData.Update( data.data, revision, function ( err )
                            {
                                return wcallback( err, revision );
                            }
                        );
                    }
                ], function ( err, revision )
                {
                    if ( err )
                    {
                        return next( err );
                    }
                    else
                    {
                        res.result = {
                            message : 'success revision : ' + revision
                        };
                    }
                    return next();
                }
            );
        }
    );

    /***
     @api
     @category dataTable
     @name get
     @method POST
     @version 1.0

     @auth false
     @desc dataTable을 얻어 온다.
     ***/
    app.post(
        '/dataTable/get',
        function ( req, res, next )
        {
            console.log( 'dataTable get..!' );

            dataTable.Model.find( {} ).sort( {revision : -1} ).limit( 1 ).read( 'p' ).exec(
                function ( err, docs )
                {
                    if ( err )
                    {
                        return next( new Error( err ) );
                    }

                    if ( !docs )
                    {
                        return next( new Error( 'empty' ) );
                    }

                    res.result = {
                        dataTable : docs[0]
                    }

                    return next();
                }
            );
        }
    );
    //
    ///**
    // @api
    // @category dataTable
    // @name getLocal
    // @method POST
    // @version 1.0
    //
    // @param characterId string required auth 확인용 캐릭터 id...
    //
    // @auth true
    // @desc client용 로컬 데이터를 얻어 온다.
    // **/
    //app.post(
    //    '/dataTable/getLocal',
    //    authentication,
    //    parseParameters( [
    //            {name : 'characterId', type : 'objectId', require : true}
    //        ]
    //    )
    //    , function ( req, res, next )
    //    {
    //        if ( req.character.characterId != req.parseParams.characterId  )
    //        {
    //            return next( new Error('invalidAuth') );
    //        }
    //        var data = localData.Get();
    //
    //        res.result = data;
    //
    //        return next();
    //    }
    //);
};