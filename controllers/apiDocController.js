/**
 * Created by Aria on 2016-07-08.
 */
//var express = require('express');
//var router = express.Router();

var parseApiParameters = require('../helper/parseApiParameters');

module.exports = function( app )
{
    app.get( '/apidoc',
        function( req, res, next )
        {
            parseApiParameters(function (apiParameters)
                {
                    res.render('apidoc', {api : apiParameters});
                }
            );
        }
    );

    app.get( '/apidoc/:category/:name',
        function ( req, res, next )
        {
            var category = req.params['category'];
            var name = req.params['name'];

            parseApiParameters( function ( apiParameters )
                {
                    if ( apiParameters[category] === undefined )
                    {
                        return next( new Error( 'invalid category' ) );
                    }

                    for ( var i = 0, max = apiParameters[category].length; i < max; ++i )
                    {
                        if ( apiParameters[category][i].name == name )
                        {
                            res.render( 'apidocTest', { api : apiParameters[category][i] } );

                            return;
                        }
                    }

                    return next( new Error( 'not exist api ' + category + name ) );
                }
            );
        }
    );

};

//router.get('/apidoc', function (req, res, next) {
//
//    res.render('apidoc', { api : {} });
//
//});
//module.exports = router;
