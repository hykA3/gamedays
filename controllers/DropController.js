/**
 * Created by Aria on 2016-07-14.
 */

var mongoose = require('mongoose');
var parseParameters = require('../helper/parseParameters');
var localData = require('../helper/localData');
var itemLocal = require('../models/localData/itemLocal');

module.exports = function (app) {
    /***
     @api
     @category bossDrop
     @name bossDropGet
     @method POST
     @version 1.0

     @param stage Number required 보스스테이지
     @param count Number required 클리어 횟수

     @auth false
     @desc Boss Clear Item Get!
     ***/

    app.post(
        '/bossDrop/bossDropGet',
        parseParameters([
                {name: 'stage', type: 'int', require: true},
                {name: 'count', type: 'int', require: true}
            ]
        ),

        function (req, res, next) {
            var cached = localData.Get().data;

            if (!cached || !cached.bossProbability) {
                return next(new Error('notFoundBossProbability'));
            }

            //스테이별 보스 아이템 json
            var bossProbability = [];
            //아이템 최대 확률
            var maxProbability = 0;

            cached.bossProbability.forEach(function (bossDropitem) {
                    if (req.body.stage >= bossDropitem.minStage && req.body.stage <= bossDropitem.maxStage) {
                        //아이템 최소 확률 ( 전아이템의 최대확률 부터 현재 아이템의 확률 + )
                        //아이템 드랍 갯수 초기화
                        var minProbability = maxProbability;
                        var count = 0;
                        maxProbability += bossDropitem.probability;

                        bossProbability.push(
                            {
                                itemCode : bossDropitem.itemCode,
                                itemName : bossDropitem.itemName,
                                minProbability: minProbability,
                                maxProbability: maxProbability,
                                count: count,
                                probability : bossDropitem.probability
                            }
                        );
                    }
                }
            );

            //Test - Boss clear count
            for (var i = 0; i < req.body.count; ++i) {
                var getProbability = Math.random() * (100.0 - 1.0) + 1.0;

                for ( var k = 0 ; k < bossProbability.length; ++k)
                {
                    var getItem = bossProbability[k];
                    if (getProbability >= getItem.minProbability && getProbability <= getItem.maxProbability) {
                        //확률에 따라 아이템 드랍.
                        //갯수를 증가시켜 업데이트 보냄
                        getItem.count++;
                    }
                }
            }
            //전송용 데이터
            res.result.item = [];
            for ( var k = 0 ; k < bossProbability.length; ++k) {

                var getItem = bossProbability[k];
                var outPutItem = {
                    itemCode : getItem.itemCode,
                    itemName : getItem.itemName,
                    count : getItem.count,
                    probability : getItem.probability
                }
                console.log(getItem);
                res.result.item.push(outPutItem);
            }
            return next(null);
        }
    );

    /***
     @api
     @category boxDrop
     @name boxDropGet
     @method POST
     @version 1.0

     @param boxType Number required 박스 종류
     @param count Number required 오픈 횟수

     @auth false
     @desc Box Open Item Get!!
     ***/

    app.post(
        '/BoxDrop/BoxDropGet',
        parseParameters([
                {name: 'boxType', type: 'int', require: true},
                {name: 'count', type: 'int', require: true}
            ]
        ),
        function(req, res, next)
        {
            var cached = localData.Get().data;

            if( !cached || !cached.boxProbability)
            {
                return next(new Error('notFoundBoxProbability'));
            }

            //입력된 박스 타입에 따라 박스 리스트 입력
            var boxList = [];
            //아이템 별 드랍 최대 확률
            var maxProbability = 0;

            //박스 타입에 따라 리스트 재생성
            cached.boxProbability.forEach(function(boxDropItem)
                {
                    if( boxDropItem.boxTypeCode == req.body.boxType )
                    {
                        //현재 아이템의 최소 확률은 이전 아이템의 최대확률부터
                        var minProbability = maxProbability;
                        var count = 0;

                        //현재 아이템의 최대 확률은 최소확률 + 아이템 드랍 확률
                        maxProbability += boxDropItem.probability;

                        boxList.push(
                            {
                                code : boxDropItem.code,
                                itemName : boxDropItem.itemName,
                                minProbability: minProbability,
                                maxProbability: maxProbability,
                                probability : boxDropItem.probability,
                                count : count
                            }
                        );
                    }
                }
            );

            //재 설정된 박스 리스트에서 아이템 드랍리스트를 만듬
            for( var i = 0; i < req.body.count; ++i )
            {
                var getProbability = Math.random() * (100.0 - 1.0) + 1.0;

                for( var j =0; j < boxList.length; ++j )
                {
                    var getItem = boxList[j];
                    if (getProbability >= getItem.minProbability && getProbability <= getItem.maxProbability) {
                        //확률에 따라 아이템 드랍.
                        //갯수를 증가시켜 업데이트 보냄
                        getItem.count++;
                    }
                }
            }
            //전송용 데이터
            res.result.item = [];
            for ( var k = 0 ; k < boxList.length; ++k) {

                var getItem = boxList[k];
                var outPutItem = {
                    code : getItem.code,
                    itemName : getItem.itemName,
                    probability : getItem.probability,
                    count : getItem.count
                }
                res.result.item.push(outPutItem);
            }
            return next(null);
        }
    );
}