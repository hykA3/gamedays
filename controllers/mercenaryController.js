/**
 * Created by Aria on 2016-07-21.
 */

var mongoose = require('mongoose');
var parseParameters = require( '../helper/parseParameters');
var localData = require('../helper/localData');

var mercenaryStatus = require('../models/mercenaryStatus');
var async = require( 'async' );

module.exports = function( app )
{
    /***
     @api
     @category mercenary
     @name getData
     @method POST
     @version 1.0

     @param deviceId string required 계정 아이디
     @param code number required 용병 코드
     @param type number required 용병 타입
     @param level number required 용병 레벨

     @auth false
     @desc Mercenary Data Get
     ***/

    app.post(
        '/mercenary/getData',
        parseParameters(
            [
                {name : 'deviceId', type : 'string', require : true },
                {name : 'code', type : 'int', require : true },
                {name : 'type', type : 'int', require : true },
                {name : 'level', type : 'int', require : true }
            ]
        ),
        function( req, res, next )
        {
            var deviceId = req.parseParams.deviceId;
            var code = req.parseParams.code;
            var type = req.parseParams.type;
            var level = req.parseParams.level;

            var cached = localData.Get().data;
            var outputMercenary = {};

            async.waterfall(
                [
                    function(wcallback )
                    {
                        mercenaryStatus.Model.find({deviceId : deviceId, code : code},
                            function (err, document)
                            {
                                if( err )
                                {
                                    return wcallback( err );
                                }

                                if( document && document.length > 0 )
                                {
                                    //res.result.mercenaryStatus = document;

                                    outputMercenary = JSON.parse(JSON.stringify(document[0]));
                                    return wcallback(null);
                                }

                                return  wcallback( 'notFountMercenary' );
                            }
                        )
                    },
                    function (wcallback)
                    {
                        //전사용병
                        if( type == 1 )
                        {
                            cached.warriorStatus.forEach(function(data)
                                {
                                    if( data.level == level )
                                    {
                                        //res.result.mercenaryData = data;
                                        outputMercenary.mercenaryData = data;
                                    }
                                }
                            )
                        }
                        //마법사용병
                        else if( type == 2 )
                        {
                            cached.magicianStatus.forEach(function(data)
                                {
                                    if( data.level == level )
                                    {
                                        //res.result.mercenaryData = data;
                                        outputMercenary.mercenaryData = data;
                                    }
                                }
                            )
                        }
                        //궁수용병
                        else if( type == 3 )
                        {
                            cached.archerStatus.forEach(function(data)
                                {
                                    if( data.level == level )
                                    {
                                        //res.result.mercenaryData = data;
                                        outputMercenary.mercenaryData = data;
                                    }
                                }
                            )
                        }
                        //잘못된 정보
                        else
                        {
                            return wcallback( new Error('notFoundType'));
                        }

                        return wcallback(null);
                    }
                ],
                function (err)
                {
                    res.result.mercenaryStatus = outputMercenary;
                    return next( err );
                }
            );
        }
    );
    /***
     @api
     @category mercenary
     @name uploadData
     @method POST
     @version 1.0

     @param deviceId string required 계정 아이디
     @param code Number required 용병 코드
     @param addExp Number required 획득경험치

     @auth false
     @desc Mercenary Data Upload
     ***/

    app.post(
        '/mercenary/uploadData',
        parseParameters(
            [
                {name : 'deviceId', type : 'string', require : true },
                {name : 'code', type : 'int', require : true },
                {name : 'addExp', type : 'int', require : true }
            ]
        ),
        function( req, res, next )
        {
            var deviceId = req.parseParams.deviceId;
            var code = req.parseParams.code;
            var addExp = req.parseParams.addExp;

            var cached = localData.Get().data;
            var outputMercenary = {};

            //현재 용병의 데이터 저장
            var currentMercenaryData = {};

            async.waterfall(
                [
                    function (wcallback)
                    {
                        //요청한 용병의 정보를 찾는다.
                        mercenaryStatus.Model.find({deviceId : deviceId, code : code },
                            function(err, document)
                            {
                                if( err )
                                {
                                    return wcallback( err );
                                }

                                if( document && document.length > 0 )
                                {
                                    currentMercenaryData = document;
                                    return wcallback(null);
                                }
                            }
                        );
                    },
                    //레벨업에 사용될 경험치를 얻어온다.
                    function( wcallback )
                    {
                        cached.experience.forEach(function (expData)
                            {
                                if( !expData && expData.length <= 0 )
                                {
                                    return wcallback( new Error('notFoundExperience'));
                                }

                                if( expData.level == currentMercenaryData.level )
                                {
                                    console.log( expData );
                                    //현재경험치 = 획득경험치 + 기존경험치
                                    var currentExp = addExp + currentMercenaryData.experience;

                                    if( currentExp >= expData.experience )
                                    {
                                        currentMercenaryData.level++;
                                        currentMercenaryData.experience = currentExp - expData.experience;

                                        return wcallback(  null );
                                    }
                                    else if( currentExp < expData.experience )
                                    {
                                        currentMercenaryData.experience = currentExp;
                                        return wcallback(  null );
                                    }
                                    else
                                    {
                                        return wcallback( new Error('wrongReckoning'));
                                    }
                                }
                            }
                        );
                    },
                    //용병 데이터 업로드드
                    function(  wcallbak )
                    {
                        mercenaryStatus.Model._findOneAndUpdate({deviceId:deviceId, code:code}, currentMercenaryData,
                            function (err, document)
                            {
                                if( err )
                                {
                                    return wcallbak( err );
                                }

                                if( document && document.length > 0 )
                                {
                                    outputMercenary = JSON.parse(JSON.stringify(document[0]));
                                    return wcallbak( null );
                                }

                                return wcallbak( new Error('failUpdate') );
                            }
                        )
                    }
                ],
                function( err )
                {
                    res.result = outputMercenary;
                    return next( err );
                }
            );
        }
    );

    /***
     @api
     @category mercenary
     @name addData
     @method POST
     @version 1.0

     @param deviceId string required 계정 아이디
     @param mercenaryType Number required 용병 타입

     @auth false
     @desc 추후 업데이트
     ***/

    app.post(
        '/mercenary/addData',
        parseParameters(
            [
                {name : 'deviceId', type : 'string', require : true },
                {name : 'type', type : 'number', require : true }
            ]
        ),
        function( req, res, next )
        {
            return next();
        }
    );

    /***
     @api
     @category mercenary
     @name deleteData
     @method POST
     @version 1.0

     @param deviceId string required 계정 아이디
     @param mercenaryCode Number required 용병 코드

     @auth false
     @desc 추후 업데이트
     ***/

    app.post(
        '/mercenary/deleteData',
        parseParameters(
            [
                {name : 'deviceId', type : 'string', require : true },
                {name : 'code', type : 'number', require : true }
            ]
        ),
        function( req, res, next )
        {
            return next();
        }
    );


}

