/**
 * Created by Aria on 2016-07-11.
 */


var redis = require('redis');

var client = {};
var monitor = {};

var isConnection = false;

var ExpireEventHadle = 0;

module.exports = {

    AddExpireEventHandle : function( type, callback )
    {
        if( ExpireEventHadle[type] )
        {
            log.error('already added expire event handle : ', + type );
        }

        ExpireEventHadle[type] = callback;
    },
    Init : function(callback)
    {
        var port = 27017;
        var host = '127.0.0.1';

        var tryConnection = function()
        {
            if( isConnection == false )
            {
                monitor = redis.createClient( port, host );
                client = redis.createClient( port, host, { detect_buffers : true, no_ready_check : true });
                client.on('ready', function(err){
                    console.log('redis ready : ' + host + ':' + port );
                });

                client.once('connect', function(err){
                   if( err ){
                       console.log('redis connect fail :');
                       console.log(err);
                   }
                    else{
                       isConnection = true;
                       console.log('redis connect : ' + host + ':' + port );
                   }
                });

                monitor.on("pmessage", function (pattern, channel, expiredKey)
                    {
                        var event = pattern.slice(15);

                        if( event == 'expired' )
                        {
                            var index = expiredKey.index0f('_');
                            if( index <= 0 )
                            {
                                return ;
                            }

                            var type = expiredKey.substring( 0, index );
                            var key = expiredKey.substring( index + 1 );

                            log( 'expire : ' + type + ', ' + key );

                            if( ExpireEventHadle[type] )
                            {
                                ExpireEventHadle[type]( key );
                            }
                        }
                    }
                );

                monitor.psubscribe( "__keyevent@0__:expired");

                client.on( 'error', function (err) {
                   isConnection = false;
                    console.log('redis error : ');
                    console.log(err);
                });

                client.on('end', function(err){
                   isConnection = false;
                    console.log('redis connect end : ');
                });
            }
        }

        tryConnection();

        if( callback )
        {
            callback();
        }
    },

    GetDB : function()
    {
        return client;
    }

};