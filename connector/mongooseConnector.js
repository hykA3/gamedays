/**
 * Created by Aria on 2016-07-07.
 */

//mongoose connect

var mongoose = require('mongoose');

mongoose.connect('mongodb://127.0.0.1:27017/dproject');

var db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'));

db.once('open', function callback(){
    console.log("mongo db connection ok.");
});

