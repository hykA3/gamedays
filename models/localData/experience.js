/**
 * Created by Aria on 2016-07-21.
 */

var mongoose = require('mongoose');

var experienceSchema = module.exports = mongoose.Schema(
    {
        code : { type : Number, default : 0 },
        level : { type : Number, default : 0 },
        experience : { type : Number, default : 0 }
    },

    {autoIndex : false, versionKey : false }
)

experienceSchema.methods.parse = function(data)
{
    this.code = data.code;
    this.level = data.level;
    this.experience = data.experience;
}

var experienceModel = mongoose.model( 'localData.experience', experienceSchema);

module.exports =
{
    Schema : experienceSchema,
    Model : experienceModel
};

