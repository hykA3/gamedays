/**
 * Created by Aria on 2016-07-20.
 */

var mongoose = require('mongoose');

var magicianStatusSchema = module.exports = mongoose.Schema(
    {
        deviceId : { type : String, default : 0 },
        code : { type : Number, default : 1 },
        level : { type : Number, default : 1 },
        hp : { type : Number, default : 35.0 },
        attackPoint : { type : Number, default : 5.0 },
        armor : { type : Number, default : 1.0 },
        moveSpeed : { type : Number, default : 50.0 },
        attackSpeed : { type : Number, default : 0.75 },
        guardChance : { type : Number, default : 1.0 },
        evasionChance : { type : Number, default : 1.0 }
    },

    {autoIndex : false, versionKey : false }

)

magicianStatusSchema.methods.parse = function( data )
{
    this.code = data.code;
    this.level = data.level;
    this.hp = data.hp;
    this.attackPoint = data.attackPoint;
    this.armor = data.armor;
    this.moveSpeed = data.moveSpeed;
    this.attackSpeed = data.attackSpeed;
    this.guardChance = data.guardChance;
    this.evasionChance = data.evasionChance;
};

var magicianStatusModel = mongoose.model( 'localData.magicianStatus', magicianStatusSchema );

module.exports = {
    Schema : magicianStatusSchema,
    Model : magicianStatusModel
}

