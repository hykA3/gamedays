/**
 * Created by Aria on 2016-07-12.
 */

var mongoose = require('mongoose');
var itemLocal = require( '../localData/itemLocal');

var bossProbabilitySchema = module.exports = mongoose.Schema(
    {
        code : { type : Number, default : 0 },
        itemCode : { type : Number, default : 0 },
        itemName : { type : String, default : null },
        minStage : { type : Number, default : 0 },
        maxStage : { type : Number, default : 0 },
        probability : { type : Number, default : 0.0 }
    },

    //options - safe, strict, capped, versionKey, autoIndex, shardKey
    {autoIndex : false, versionKey : false }
);

bossProbabilitySchema.methods.parse = function( data )
{
    this.code = data.code;
    this.itemCode = data.itemCode;
    this.itemName = data.itemName;
    this.minStage = data.minStage;
    this.maxStage = data.maxStage;
    this.probability = data.probability;
};

var bossProbabilityModel = mongoose.model('localData.bossProbability', bossProbabilitySchema);

module.exports = {
    Schema : bossProbabilitySchema,
    Model : bossProbabilityModel
};
