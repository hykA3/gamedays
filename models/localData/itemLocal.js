/**
 * Created by Aria on 2016-07-18.
 */

var mongoose = require('mongoose');

var itemLocalSchema = module.exports = mongoose.Schema(

    {
        code : { type : Number, default : 0},
        itemTypeCode : { type : Number, default : 0},
        itemTypeName : { type : String, default : null },
        itemCode : { type : Number, default : 0 },
        itemCodeName : { type : String, default : null },
        buble : { type : Boolean, default : false},
        salable : { type : Boolean, default : true },
        stackable : { type : Boolean, default : false }
    },

    {autoIndex : false, versionKey : false }

)

itemLocalSchema.methods.parse = function( data )
{
    this.code = data.code;
    this.itemTypeCode = data.itemTypeCode;
    this.itemTypeName = data.itemTypeName;
    this.itemCode = data.itemCode;
    this.itemCodeName = data.itemCodeName;
    this.buble = data.buble;
    this.salable = data.salable;
    this.stackable = data.stackable;
};

var itemLocalModel = mongoose.model( 'localData.item', itemLocalSchema );

module.exports = {
    Schema : itemLocalSchema,
    Model : itemLocalModel
}