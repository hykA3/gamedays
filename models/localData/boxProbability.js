/**
 * Created by Aria on 2016-07-19.
 */

var mongoose = require('mongoose');
var itemLocal = require( '../localData/itemLocal');

var boxProbabilitySchema = module.exports = mongoose.Schema(
    {
        code : { type : Number, default : 0 },
        itemCode : { type : Number, default : 0 },
        itemName : { type : String, default : null },
        boxTypeCode : { type : Number, default : 0 },
        boxTypeName : { type : String, default : null },
        probability : { type : Number, default : 0 }
    },

    //options - safe, strict, capped, versionKey, autoIndex, shardKey
    {autoIndex : false, versionKey : false }
);

boxProbabilitySchema.methods.parse = function( data )
{
    this.code = data.code;
    this.itemCode = data.itemCode;
    this.itemName = data.itemName;
    this.boxTypeCode = data.boxTypeCode;
    this.boxTypeName = data.boxTypeName;
    this.probability = data.probability;
};

var boxProbabilityModel = mongoose.model('localData.boxProbability', boxProbabilitySchema);

module.exports = {
    Schema : boxProbabilitySchema,
    Model : boxProbabilityModel
};

