/**
 * Created by Aria on 2016-07-19.
 */

var mongoose = require( 'mongoose' );

var boxProbabilitySchema = module.exports = mongoose.Schema(
    {
        code : { type : Number, default : 0 },
        name : { type : String, default: null }
    },

    {autoIndex : false, versionKey : false }

);

boxProbabilitySchema.methods.parse = function (data)
{
    this.code = data.code;
    this.name = data.name;
};

var boxProbailityModel = mongoose.model( 'localData.boxType', boxProbabilitySchema );

module.exports =
{
    Schema : boxProbabilitySchema,
    Model : boxProbailityModel
}
