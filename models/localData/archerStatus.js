/**
 * Created by Aria on 2016-07-20.
 */

var mongoose = require('mongoose');

var archerStatusSchema = module.exports = mongoose.Schema(
    {
        deviceId : { type : Number, default : 0 },
        code : { type : Number, default : 1 },
        level : { type : Number, default : 1 },
        hp : { type : Number, default : 40.0 },
        attackPoint : { type : Number, default : 2.0 },
        armor : { type : Number, default : 1.0 },
        moveSpeed : { type : Number, default : 60.0 },
        attackSpeed : { type : Number, default : 1.25 },
        guardChance : { type : Number, default : 0.0 },
        evasionChance : { type : Number, default : 1.0 }
    },

    {autoIndex : false, versionKey : false }

)

archerStatusSchema.methods.parse = function( data )
{
    this.code = data.code;
    this.level = data.level;
    this.hp = data.hp;
    this.attackPoint = data.attackPoint;
    this.armor = data.armor;
    this.moveSpeed = data.moveSpeed;
    this.attackSpeed = data.attackSpeed;
    this.guardChance = data.guardChance;
    this.evasionChance = data.evasionChance;
};

var archerStatusModel = mongoose.model( 'localData.archerStatus', archerStatusSchema );

module.exports = {
    Schema : archerStatusSchema,
    Model : archerStatusModel
}
