/**
 * Created by Aria on 2016-07-19.
 */

var mongoose = require('mongoose');

var characterSchema = module.exports = mongoose.Schema(
    {
        deviceId : { type : String, default : null, index : true},  //유저(디바이스) id
        nickName : { type : String, default : null },                   //닉네임
        ruby : { type : Number, default : 0 },                          //루비
        money : { type : Number, default : 0 },                         //소지금
        stage : { type : Number, default : 0 }                         //스테이지
    }
);

var characterModel = mongoose.model('users', characterSchema);

module.exports = {
    Schema : characterSchema,
    Model : characterModel
}