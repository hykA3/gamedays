/**
 * Created by Aria on 2016-07-20.
 */

var mongoose = require('mongoose');

var mercenaryStatusSchema = module.exports = mongoose.Schema(
    {
        deviceId : { type : String, default : null },       //유저 디바이스 값
        code : { type : Number, default : 0 },     //용병의 고유 코드
        type : { type : Number, default : 0},      //용병의 종류
        level : { type : Number, default : 1},         //용병의 레벨
        experience : { type : Number, default : 0 }                //용병 경험치
    }
);

var mercenaryStatusModel = mongoose.model( 'mercenaryStatus', mercenaryStatusSchema );

module.exports = {
    Schema : mercenaryStatusSchema,
    Model : mercenaryStatusModel
}