/**
 * Created by Aria on 2016-07-12.
 */

var mongoose = require( 'mongoose' );

var dataTableSchema = module.exports = mongoose.Schema( {
        revision : { type : Number, default : null }
        , data : { type : mongoose.Schema.Types.Mixed, default : null }
    },

    //options - safe, strict, capped, versionKey, autoIndex, shardKey
    {autoIndex : false, versionKey : false}
);


var dataTableModel = mongoose.model( 'data', dataTableSchema );

module.exports = {
    Schema : dataTableSchema,
    Model : dataTableModel
};
